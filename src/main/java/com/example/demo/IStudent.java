package com.example.demo;

import java.util.List;

public interface IStudent {
    List<Student> findAll();
}
