package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/api")
public class ExampleController {

    @Autowired
    IStudent iStudent;
    @GetMapping("all")
    public List<Student> getAllStudent(){
        return iStudent.findAll();
    }
}
