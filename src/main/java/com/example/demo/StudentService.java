package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService implements IStudent{

    @Override
    public List<Student> findAll() {
        List<Student> list = new ArrayList<>();
        Student student = new Student();
        student.setName("Truong");
        student.setAddress("NA");
        student.setAge("10");
        Student student1 = new Student();
        student1.setName("Truong1");
        student1.setAddress("NA1");
        student1.setAge("101");
        list.add(student);
        list.add(student1);
        return list;
    }
}
